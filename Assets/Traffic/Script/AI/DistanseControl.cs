﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanseControl : MonoBehaviour
{
    [SerializeField] CarController car; // сериализованная ссылка на класс нашего авто

    // срабатывет когда в коллайдер входит объект в котором есть компонент CarController
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CarController>() != null) 
        {
            car.stop = true; // останавливает транспорт если дистация опасная 
        }
    }

    // срабатывет когда в коллайдер выходит объект в котором есть компонент CarController
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CarController>() != null)
        {
            car.stop = false;           
        }
    }

}
