﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarController : MonoBehaviour
{
    [SerializeField] Transform[] targetPosition; // координаты точек по которым ведется патрулирование
    NavMeshAgent car; // ссылка на объект агента


    [SerializeField] float _speed = 5f; // скорость агента во аремя бега или приследования
    [SerializeField] public bool stop; // должен ли остановаться транспорт
    [SerializeField] int target; // текущая точка маршрута

    // Start is called before the first frame update
    void Start()
    {    
        car = GetComponent<NavMeshAgent>();  // кешируем компонет агента     
        car.speed = _speed; // назначаем скорость движения
        GoToTarget(); // задаем первую точку маршрута
    }

    private void Update()
    {
        //если стоп активна останавливаем транспорт
        if (stop)
        {
            car.speed = 0;
        }
        else 
        {
            car.speed = _speed;
        }

        //если агент ближе чем 1.5 юнита ближе к точке то задается новая из массива, если точка последняя то переходит к нулевой
        if (car.remainingDistance < 1.5f) 
        {
            if (target < targetPosition.Length-1)
            {
                target += 1;
            }
            else 
            {
                target = 0;
            }
            GoToTarget();
        }   
    }

    public void GoToTarget() 
    {
        car.destination = targetPosition[target].position; // навмеш агенту назначется точка маршрута
    }
}

