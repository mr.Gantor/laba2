﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightControll : MonoBehaviour
{
    [SerializeField] Light[] traffLight; // объекты с лучами 
    [SerializeField] StopCar[] stopper; // зоны для остановки
    [SerializeField] GameObject[] cube; // оюъекты кубов
    [SerializeField] Material[] liter; // материалы для перключения цвета кубов
    [SerializeField] bool step;

    private void Start()
    {
        Invoke("change", 3f); // вызов метода через три секунды
    }

    public void change() 
    {
        if (step)
        {
            // смена цвета лучей
            traffLight[0].color = new Color32(65, 255, 0, 255);
            traffLight[1].color = new Color32(65, 255, 0, 255);
            traffLight[2].color = new Color32(255, 0, 0, 255);
            // меняем материал у кубов
            cube[0].GetComponent<MeshRenderer>().material = liter[0];
            cube[1].GetComponent<MeshRenderer>().material = liter[0];
            cube[2].GetComponent<MeshRenderer>().material = liter[1];
            // менем флаг у зон
            stopper[0].stopping = false;
            stopper[1].stopping = false;
            stopper[2].stopping = true;
        }
        else 
        {
            traffLight[0].color = new Color32(255, 0, 0, 255);
            traffLight[1].color = new Color32(255, 0, 0, 255);
            traffLight[2].color = new Color32(65, 255, 0, 255);
            cube[0].GetComponent<MeshRenderer>().material = liter[1];
            cube[1].GetComponent<MeshRenderer>().material = liter[1];
            cube[2].GetComponent<MeshRenderer>().material = liter[0];
            stopper[0].stopping = true;
            stopper[1].stopping = true;
            stopper[2].stopping = false;
        }
        step = !step;
        Invoke("change", 3f); // повторно метод вызывает себя через 3 секунды
    }
}
