﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopCar : MonoBehaviour
{
    [SerializeField] public bool stopping; // должен ли останавливаться транспорт в зоне

    // транспорт на слое 9 останавливается на входе 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            other.GetComponent<CarController>().stop = true;
        } 
    }

    // находясь в зоне транспорт получает команду останавливаться или ехать в зависимости от флага
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            other.GetComponent<CarController>().stop = stopping;
        }
    }
}
